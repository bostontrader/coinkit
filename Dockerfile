FROM alpine:3.9

RUN apk update
RUN apk add g++ gdb python3-dev
RUN pip3 install gdbgui

RUN apk add boost-dev
RUN pip3 install gcovr


